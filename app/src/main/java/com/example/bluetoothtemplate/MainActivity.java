package com.example.bluetoothtemplate;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    //Establecer los elementos que se van a usar
    CheckBox visible_bt;
    TextView nombre_bt;
    ListView ListView_bt;

    //BA Obtiene el Adaptador Bluetooth Elemento - Nombre - Valor
    private BluetoothAdapter BA = BluetoothAdapter.getDefaultAdapter();
    Set<BluetoothDevice> DispositivosEmparejados = BA.getBondedDevices();

    //Variable que almacena la MAC del dispositivo para pasarla al siguiente activity
    public static String DireccionBT = "device_address";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //CONEXIÓN ENTRE LA PARTE GRÁFICA Y LA LÓGICA
        visible_bt = findViewById(R.id.chk_Visible);
        nombre_bt = findViewById(R.id.txt_NombreDeBluetooth);
        ListView_bt = findViewById(R.id.ListViewBluetooth);

        //Setear los nombres de los dispositivos Bluetooth
        nombre_bt.setText(obtenerNombreBluetoothLocal());

        //Si el Adaptador Bluetooth está vacío:
        if (BA == null) {
            Toast.makeText(this, "Bluetooth no soportado", Toast.LENGTH_SHORT).show();
            finish();
        }

        //Si el Adaptador Bluetooth está conectado:
        if (BA.isEnabled()) {
            encenderBT();
            listaDeDispositivos();
        } else {
            Toast.makeText(this, "El dispositivo Bluetooth está desconectado", Toast.LENGTH_SHORT).show();        }

        //Método para la visibilidad del dispositivo
        /*visible_bt.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Intent getVisible = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE); //Preguntamos al usuario si desea hacerse visible por 2 segundos
                    startActivityForResult(getVisible, 0); //Inicia la actividad para hacerse visible
                    Toast.makeText(MainActivity.this, "Visible por dos minutos", Toast.LENGTH_SHORT).show();
                }
            }
        });*/
    }

    //Método para encender el BT
    private void encenderBT() {
        BA.enable();
        Toast.makeText(MainActivity.this, "Bluetooth encendido", Toast.LENGTH_SHORT).show();
    }

    //Método para mostrar los dispositivos en la lista
    private void listaDeDispositivos() {
        ArrayList<String> lista = new ArrayList<>();
        if (DispositivosEmparejados != null) {
            for (BluetoothDevice dispositivo : DispositivosEmparejados) {
                //Paso los datos a String
                String nombreDispositivo = dispositivo.getName();
                String direccionDispositivo = dispositivo.getAddress();
                lista.add(nombreDispositivo + "\n" + direccionDispositivo);
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, lista);
            ListView_bt.setAdapter(adapter);
            ListView_bt.setOnItemClickListener(adaptadorLista);
        } else {
            Toast.makeText(this, "No se encontraron dispositivos vinculados", Toast.LENGTH_SHORT).show();
        }
    }

    //Método para obtener el nombre del dispositivo
    @SuppressLint("HardwareIds")
    public String obtenerNombreBluetoothLocal() {
        if (BA == null) {
            BA = BluetoothAdapter.getDefaultAdapter();
        }

        String nombre = BA.getName();
        if (nombre == null) {
            nombre = BA.getAddress();
        }

        return nombre;
    }

    //OnClick de la lista de BT
    private AdapterView.OnItemClickListener adaptadorLista = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            //Obtener el ID del ítem seleccionado
            String itemSeleccionado = String.valueOf(ListView_bt.getItemAtPosition(position));

            //Obtener la dirección MAC del dispositivo (Parseado)
            String direccion = itemSeleccionado.substring(itemSeleccionado.length() - 17);
            Toast.makeText(MainActivity.this, "Conectando a " + itemSeleccionado, Toast.LENGTH_SHORT).show();

            //Enviar la dirección a la siguiente activity
            Intent i = new Intent(MainActivity.this, Main2Activity.class);
            i.putExtra(DireccionBT, direccion);
            startActivity(i);
        }
    };
}