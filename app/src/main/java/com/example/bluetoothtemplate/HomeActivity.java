package com.example.bluetoothtemplate;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class HomeActivity extends AppCompatActivity {

    TextView txt_Bienvenida;
    Button btn_ConectarBT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        txt_Bienvenida = findViewById(R.id.txt_Bienvenida);
        btn_ConectarBT = findViewById(R.id.btn_ConectarBT);


        btn_ConectarBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent habilitarBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE); //Preguntamos al usuario si desea encender el bluetooth
                startActivityForResult(habilitarBtIntent, 1); //Inicia la actividad para encender el Bluetooth
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            Toast.makeText(this, "TODO OKEY!", Toast.LENGTH_SHORT).show();

            String conectarvalue = "conectarSi";
            Intent i = new Intent(HomeActivity.this, MainActivity.class);
            i.putExtra("conectarvalue", conectarvalue);
            startActivity(i);
        }
    }
}
