package com.example.bluetoothtemplate;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.UUID;

public class Main2Activity extends AppCompatActivity {

    //Establecer los elementos que se van a usar
    TextView txt_NombreDispositivoConectado, txt_progreso;
    CheckBox chk_1, chk_2;
    Button btn_Volver, btn_uno, btn_dos;
    Vibrator vibrador;
    SeekBar seekbar;
    BluetoothAdapter myBluetooth = null;
    BluetoothSocket enlaceBT = null;

    //Flag para isBTConnected
    private boolean isBtConnected = false;
    //Variable con la UUID (Universal Unique ID)
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    //Variables
    String comandoUnoON = "a";
    boolean UnoON = false;
    String comandoUnoOFF = "b";
    String comandoDosON = "c";
    boolean DosON = false;
    String comandoDosOFF = "d";
    String direccion = null;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        //CONEXIÓN ENTRE LA PARTE GRÁFICA Y LA LÓGICA
        txt_NombreDispositivoConectado = findViewById(R.id.txt_NombreDispositivoConectado);
        btn_Volver = findViewById(R.id.btn_Volver);
        btn_uno = findViewById(R.id.btn_uno);
        btn_dos = findViewById(R.id.btn_dos);
        chk_1 = findViewById(R.id.chk_1);
        chk_2 = findViewById(R.id.chk_2);
        seekbar = findViewById(R.id.sk_1);
        txt_progreso = findViewById(R.id.progreso);

        //Recibimos la MAC address obtenida en la actividad anterior
        direccion = getIntent().getStringExtra(MainActivity.DireccionBT);

        //Llama al método ConectarBT
        new ConectarBT().execute();
        vibrador = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        btn_uno.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    enviarMensajeUno(comandoUnoON);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    enviarMensajeUno(comandoUnoOFF);
                }
                vibrador.vibrate(100);
                return true;
            }
        });

        //Presionar y levantar
        btn_dos.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    enviarMensajeDos(comandoDosON);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    enviarMensajeDos(comandoDosOFF);
                }
                vibrador.vibrate(100);
                return true;
            }
        });

        //Checkbox 1
        chk_1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (chk_1.isChecked()) {
                    enviarMensajeUno(comandoUnoON);
                    chk_1.setText(getResources().getString(R.string.str_activado));
                } else {
                    enviarMensajeUno(comandoUnoOFF);
                    chk_1.setText(getResources().getString(R.string.str_desactivado));
                }
                vibrador.vibrate(100);
            }
        });

        //CheckBox 2
        chk_2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (chk_2.isChecked()) {
                    enviarMensajeDos(comandoDosON);
                    chk_2.setText(getResources().getString(R.string.str_activado));
                } else {
                    enviarMensajeDos(comandoDosOFF);
                    chk_2.setText(getResources().getString(R.string.str_desactivado));
                }
                vibrador.vibrate(100);
            }
        });

        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                String progreso = String.valueOf(progress);
                txt_progreso.setText(progreso);
                if (progress <= 40) {
                    if (!UnoON) {
                        enviarMensajeUno(comandoUnoON);
                        UnoON = true;
                    }
                }
                if (progress >= 60) {
                    if (!DosON) {
                        enviarMensajeDos(comandoDosON);
                        DosON = true;
                    }
                }
                if (progress > 40 && progress < 60) {
                    if (UnoON) {
                        UnoON = false;
                        enviarMensajeUno(comandoUnoOFF);
                    }
                    if (DosON) {
                        DosON = false;
                        enviarMensajeDos(comandoDosOFF);
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                seekbar.setProgress(50);
            }
        });

        //Botón Volver
        btn_Volver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    //Método para producir un mensaje reutilizando un Toast
    private void msg(String Mensaje) {
        Toast.makeText(getApplicationContext(), Mensaje, Toast.LENGTH_LONG).show();
    }

    private void enviarMensajeUno(String estado) {
        //SI EL ENLACE ESTÁ ACTIVO:
        if (enlaceBT != null) {
            //COMANDO PARA ENVIAR DATOS EN FORMATO BYTE
            try {
                enlaceBT.getOutputStream().write(estado.getBytes());
                txt_progreso.setText(estado);
            } catch (IOException e) {
                msg("Error");
            }
        }
    }

    private void enviarMensajeDos(String estado) {
        if (enlaceBT != null) {
            //COMANDO PARA ENVIAR DATOS EN FORMATO BYTE
            try {
                enlaceBT.getOutputStream().write(estado.getBytes());
                txt_progreso.setText(estado);
            } catch (IOException e) {
                msg("Error");
            }
        }
    }

    //Tarea asincrónica (Permite ejecutar acciones en segundo plano)
    @SuppressLint("StaticFieldLeak")
    private class ConectarBT extends AsyncTask<Void, Void, Void> { // UI thread
        private boolean ConnectSuccess = true;

        @Override
        protected Void doInBackground(Void... devices) {
            try {
                if (enlaceBT == null || !isBtConnected) {
                    //Establece el adaptador Bluetooth a la variable myBluetooth
                    myBluetooth = BluetoothAdapter.getDefaultAdapter();
                    //Conectamos al dispositivo y chequeamos si esta disponible
                    BluetoothDevice dispositivo = myBluetooth.getRemoteDevice(direccion);

                    //Posible lazo de conexión con el dispositivo
                    enlaceBT = dispositivo.createInsecureRfcommSocketToServiceRecord(myUUID);

                    //Cancelar descubrimiento
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery();

                    //Conexión con el lazo
                    enlaceBT.connect();
                }
            } catch (IOException e) {
                ConnectSuccess = false; //Cambiar a False para conectar con Bluetooth
            }
            return null;
        }

        //Resultado después de la conexión
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (!ConnectSuccess) {
                msg("Conexión Fallida");
                finish();
            } else {
                msg("Conectado");
                isBtConnected = true;
            }
        }
    }
}